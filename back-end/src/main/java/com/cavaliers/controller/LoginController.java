package com.cavaliers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cavaliers.entity.AppUser;
import com.cavaliers.repository.AppUserRepository;

@RestController

public class LoginController {
	
	@Autowired
	private AppUserRepository appUserRepository;
	
	
	@GetMapping("/")
	public List<AppUser> hello() {
		return appUserRepository.findAll();
	}
	@PostMapping("/register")
	public AppUser addUser(@RequestBody AppUser appUser){
		
		return appUserRepository.save(appUser);

	}

}
