package com.cavaliers.service;

import org.springframework.stereotype.Service;

import java.util.Collections;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		PasswordEncoder encoder= PasswordEncoderFactories.createDelegatingPasswordEncoder();
		String defaultUserName = "wardani.zied@gmail.com";
		String password=encoder.encode("password");
		
		if(!defaultUserName.equalsIgnoreCase(userName)) {
			throw new UsernameNotFoundException("User Not Found");
		}
		
		UserDetails userDetails= User.builder()
				.username(userName)
				.password(password)
				.authorities(Collections.emptyList())
				.build();		
		return userDetails;
	}

}
