package com.cavaliers.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.stereotype.Component;

import com.cavaliers.entity.AppUser;
import com.cavaliers.entity.Post;
import com.cavaliers.repository.AppUserRepository;
import com.cavaliers.repository.PostRepository;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class InitTestData implements ApplicationListener<ApplicationContextEvent> {
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private AppUserRepository appUserRepository;
	
	
	@Override
	public void onApplicationEvent(ApplicationContextEvent event) {
		
		postRepository.save(new Post(0,"Post title nr1","Post content nr1"));
		postRepository.save(new Post(0,"Post title nr2","Post content nr2"));
		postRepository.save(new Post(0,"Post title nr3","Post content nr3"));
		log.info("3 posts saved");
		
		appUserRepository.save( new AppUser("zied@zied","zied", "wardani", "a", "a"));
		log.info(" User saved.");
		appUserRepository.save(new AppUser("zied@zied","zied", "wardani", "a", "a"));
		log.info(" User saved.");
	}
	

	
}
