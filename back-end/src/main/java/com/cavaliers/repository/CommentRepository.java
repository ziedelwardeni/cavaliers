package com.cavaliers.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cavaliers.entity.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

	List<Comment> findCommentByPostId(Integer postId);
}
