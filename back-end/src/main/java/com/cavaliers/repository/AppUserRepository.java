package com.cavaliers.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cavaliers.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, String> {
	public AppUser findByEmail(String email);

}
