package com.cavaliers.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="app_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUser {
	
	@Column(unique = true)
	@Email(message = "Email should be valid")
	private String email;
	
	@NotBlank(message = "first name should not be blank")
	private String firstName;
	
	@NotBlank(message = "last name should not be blank")
	private String lastName;
	
	@NotBlank(message = "a password should be provided")
	private String password;
	@NotBlank(message = "a password confirmation should be provided")
	private String passwordConfirm;
	
	@Column(nullable = false)
	private String roles="USER";

	public AppUser(
			String email,
			 String firstName,
			 String lastName,
			String password,
			String passwordConfirm) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.passwordConfirm = passwordConfirm;
	}
	
	
}
